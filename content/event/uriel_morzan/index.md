---
title: 'LADE Seminar'
subtitle: |
  Uriel Morzan (Universidad de Buenos Aires)

  **Title**: Temperature, light and proteins:the secret triggers of bioactivity through the lens of atomistic simulations

event: LADE Seminar
event_url: 


location: Meeting Room S-20, Building C, Area Science Park
address:
  street: Località Padriciano 99
  city: Trieste
  region: 
  postcode: '34149'
  country: Italy

summary: Uriel Morzan (Universidad de Buenos Aires)
abstract: "Over the last 3 billion years, the molecular components of life have evolved to become some of the most efficient agents for the interconversion of thermal, light, and chemical energy known to date. In this seminar, I will show how the combination of electronic structure methods, molecular dynamics, and data science can help unveil some of the key aspects of this efficiency, thus laying the groundwork for designing biocompatible devices with unprecedented optical and catalytic properties."

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: '2024-05-29T11:30:00Z'
date_end: '2024-05-29T12:30:00Z'
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: '2018-01-01T00:00:00Z'

authors: []
tags: ["Recent","LADE","Seminar"]

# Is this a featured talk? (true/false)
featured: false

image:
  caption: ''
  focal_point: Right

url_code: ''
url_pdf: ''
url_slides: ''
url_video: ''

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides:

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects:
---

<!-- Slides can be added in a few ways:

- **Create** slides using Wowchemy's [_Slides_](https://wowchemy.com/docs/managing-content/#create-slides) feature and link using `slides` parameter in the front matter of the talk file
- **Upload** an existing slide deck to `static/` and link using `url_slides` parameter in the front matter of the talk file
- **Embed** your slides (e.g. Google Slides) or presentation video on this page using [shortcodes](https://wowchemy.com/docs/writing-markdown-latex/).

Further event details, including page elements such as image galleries, can be added to the body of this page. -->
