---
title: 'LADE Seminar'
subtitle: |
  Olivier Languin-Cattoën (SISSA)

  **Title**: Diffusion Models for modeling RNA dynamics

event: LADE Seminar
event_url: 


location: Meeting Room S-20, Building C, Area Science Park
address:
  street: Località Padriciano 99
  city: Trieste
  region: 
  postcode: '34149'
  country: Italy

summary: Olivier Languin-Cattoën (SISSA)
abstract: "Diffusion Models (DMs) have become a central paradigm in generative modeling. They have attained unprecedented performances on multiple complex generative tasks such as image denoising, conditional image synthesis or temporal data modeling, surpassing the long-time prevailing generative adversarial networks (GANs) both in ease-of-training and diversity of generated samples. For these reasons, DMs are now being increasingly applied to the task of molecular conformational prediction and sampling.
In this presentation we aim to provide an overview of the recent advances in applying DMs to molecular conformational sampling with a particular emphasis on RNA. Compared to the well-studied proteins, RNA presents unique challenges when it comes to structural prediction, including scarcity of data and a highly dynamic conformational landscape. We will present some of our thoughts on designing a diffusion-based approach to the task of RNA conformational ensemble prediction using a combination of coarse-graining and torsional diffusion, a variant of diffusion adapted to internal molecular coordinates. Our goal will be to elicit an open discussion about the promises and challenges of DMs applied to the modeling of RNA dynamics."

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: '2024-05-22T11:30:00Z'
date_end: '2024-05-22T12:30:00Z'
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: '2018-01-01T00:00:00Z'

authors: []
tags: ["Recent","LADE","Seminar"]

# Is this a featured talk? (true/false)
featured: false

image:
  caption: ''
  focal_point: Right

url_code: ''
url_pdf: ''
url_slides: ''
url_video: ''

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides:

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects:
---

<!-- Slides can be added in a few ways:

- **Create** slides using Wowchemy's [_Slides_](https://wowchemy.com/docs/managing-content/#create-slides) feature and link using `slides` parameter in the front matter of the talk file
- **Upload** an existing slide deck to `static/` and link using `url_slides` parameter in the front matter of the talk file
- **Embed** your slides (e.g. Google Slides) or presentation video on this page using [shortcodes](https://wowchemy.com/docs/writing-markdown-latex/).

Further event details, including page elements such as image galleries, can be added to the body of this page. -->
