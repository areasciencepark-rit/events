---
title: 'RIT Seminar'
subtitle: |
  Ornella Affinito, SYNLAB - IRCSS SDN 

  **Title**: "(Epi-)genomic approaches in physiological and pathological systems"

event: LAGE Seminar
event_url: https://teams.microsoft.com/l/meetup-join/19%3ameeting_YmJiNDVhMjgtYjE1Zi00OWM0LWFmY2ItZjBmZmM3YWY4YzAz%40thread.v2/0?context=%7b%22Tid%22%3a%22d4aafca6-bf35-4515-b06a-4973cdfbbed3%22%2c%22Oid%22%3a%22434a6cde-0b76-44a2-859c-b1123d743160%22%7d

location: Seminar Room, st. 158AM, Area Science Park, Padriciano - Trieste + web Seminar

summary: Ornella Affinito, SYNLAB - IRCSS SDN 
abstract: "TBD"

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: '2024-07-12T10:30:00Z'
date_end: '2024-07-12T13:00:00Z'
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: '2018-01-01T00:00:00Z'

authors: []
tags: ["Upcoming","LAGE","Seminar"]

# Is this a featured talk? (true/false)
featured: false

image:
  caption: ''
  focal_point: Right

url_code: ''
url_pdf: ''
url_slides: ''
url_video: ''

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides:

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects:
---

<!-- Slides can be added in a few ways:

- **Create** slides using Wowchemy's [_Slides_](https://wowchemy.com/docs/managing-content/#create-slides) feature and link using `slides` parameter in the front matter of the talk file
- **Upload** an existing slide deck to `static/` and link using `url_slides` parameter in the front matter of the talk file
- **Embed** your slides (e.g. Google Slides) or presentation video on this page using [shortcodes](https://wowchemy.com/docs/writing-markdown-latex/).

Further event details, including page elements such as image galleries, can be added to the body of this page. -->