---
title: 'Laureandi Day'

# event: Seminar
# event_url: 

location: Meeting Room, building C1
address:
  street: Area Science Park, Località Padriciano 99
  city: Trieste
  region: 
  postcode: '34149'
  country: Italy

summary: Francesco Ortu, Giada Panerai, Isac Pasianotto, Alessandro Pietro Serra
# abstract: "TBD"

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: '2024-06-25T10:00:00Z'
date_end: '2024-06-25T13:00:00Z'
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: '2018-01-01T00:00:00Z'

authors: []
tags: ["Recent","Seminar"]

# Is this a featured talk? (true/false)
featured: false

image:
  caption: ''
  focal_point: Right

url_code: ''
url_pdf: ''
url_slides: ''
url_video: ''

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides:

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects:
---

## Program


10:00 - 10:30: Talk by ***Francesco Ortu, Master Degree in Data Science and Scientific Computing***

**Title**: "Interpreting How Large Language Models Handle Facts and Counterfactuals through Mechanistic Interpretability"

10:30 - 11:00: Talk by ***Alessandro Pietro Serra, Master Degree in Data Science and Scientific Computing***

**Title**: "A Geometric Interpretation of Few Shot Learning and Finetuning in Language Model Representations"

11:00 - 11:30: ***Coffee Break***

11:30 - 12:00: Talk by ***Giada Panerai, Dipartimento di Matematica, Informatica e Geoscienze***

**Title**: "Zig-Zag Persistence in Neural Networks Representations"

12:00 - 12:30: Talk by ***Isac Pasianotto, Master Degree in Data Science and Scientific Computing***

**Title**: "New approaches in scientific computing: the design of a cloud-ready HPC infrastructure"

<!-- Slides can be added in a few ways:

- **Create** slides using Wowchemy's [_Slides_](https://wowchemy.com/docs/managing-content/#create-slides) feature and link using `slides` parameter in the front matter of the talk file
- **Upload** an existing slide deck to `static/` and link using `url_slides` parameter in the front matter of the talk file
- **Embed** your slides (e.g. Google Slides) or presentation video on this page using [shortcodes](https://wowchemy.com/docs/writing-markdown-latex/).

Further event details, including page elements such as image galleries, can be added to the body of this page. -->
