---
title: 'LADE Meeting'
subtitle: |
  Niccolò Tosato

  **Title**: Emergent representations in networks trained with the Forward-Forward algorithm

event: LADE Meeting
event_url: 

location: Sala Riunioni RIT, Area Science Park
address:
  street: Località Padriciano 99
  city: Trieste
  region: 
  postcode: '34149'
  country: Italy

summary: Niccolò Tosato
abstract: "This talk presents new findings on the Forward-Forward algorithm, demonstrating that it can spontaneously organize internal representations into category-specific, highly sparse ensembles. These emergent patterns are strikingly similar to neuronal ensembles observed in cortical sensory areas, which are thought to be essential for perception and action."

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: '2024-06-18T11:30:00Z'
date_end: '2024-06-18T12:30:00Z'
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: '2018-01-01T00:00:00Z'

authors: []
tags: ["Recent","LADE","Meeting"]

# Is this a featured talk? (true/false)
featured: false

image:
  caption: ''
  focal_point: Right

url_code: ''
url_pdf: ''
url_slides: ''
url_video: ''

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides:

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects:
---
