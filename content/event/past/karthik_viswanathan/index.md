---
title: 'LADE Meeting: Karthik Viswanathan (UvA)'
subtitle: Information Maximizing Persistent Homology

event: LADE Meeting
event_url: 

location: Sala Riunioni RIT, Area Science Park
address:
  street: Località Padriciano 99
  city: Trieste
  region: 
  postcode: '34149'
  country: Italy

summary: Karthik Viswanathan (UvA)
abstract: "Persistent homology (PH) is a mathematical tool in computational topology that measures the topological features of data such as loops and voids that persist across multiple scales with applications ranging from biological networks to cosmology. We represent the data in the form of a persistent diagram that keeps track of the length scales at which the topological features are created and destroyed. It is possible that we lose information about the dataset while we construct the persistence diagram. In this talk, I'll explore how to quantify the information content of the persistent diagram with respect to the dataset. It can then be seen that the hyperparameters used to build this filtered simplicial complex can be optimized to maximize the Fisher information of the corresponding persistence summaries. This algorithm may be useful for integrating topological information into statistical inference. In an experimental setup, we show that in some cases topology can provide optimal summaries, in the sense that the Cramer-Rao bound is saturated."

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: '2023-09-20T11:30:00Z'
date_end: '2023-09-20T12:30:00Z'
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: '2018-01-01T00:00:00Z'

authors: []
tags: ["LADE","Meeting"]

# Is this a featured talk? (true/false)
featured: false

image:
  caption: ''
  focal_point: Right

url_code: ''
url_pdf: ''
url_slides: ''
url_video: ''

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides:

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects:
---

<!-- Slides can be added in a few ways:

- **Create** slides using Wowchemy's [_Slides_](https://wowchemy.com/docs/managing-content/#create-slides) feature and link using `slides` parameter in the front matter of the talk file
- **Upload** an existing slide deck to `static/` and link using `url_slides` parameter in the front matter of the talk file
- **Embed** your slides (e.g. Google Slides) or presentation video on this page using [shortcodes](https://wowchemy.com/docs/writing-markdown-latex/).

Further event details, including page elements such as image galleries, can be added to the body of this page. -->
