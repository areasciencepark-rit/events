---
title: 'LADE Meeting'
subtitle: |
  Yuri Gardinazzi

  **Title**: Sensory-loop adaptation in Boolean network robots

event: LADE Meeting
event_url: 

location: Sala Riunioni RIT, Area Science Park
address:
  street: Località Padriciano 99
  city: Trieste
  region: 
  postcode: '34149'
  country: Italy

summary: Yuri Gardinazzi
abstract: "Recent technological advances have made it possible to produce tiny robots equipped with simple sensors and effectors. Micro-robots are particularly suitable for scenarios such as exploration of hostile environments, and emergency intervention –e.g. in areas subject to earthquakes or fires. A crucial desirable feature of such a robot is the capability of adapting to the specific environment in which it has to operate. Given the limited computational capabilities of a micro-robot, this property cannot be achieved by complicated software but it rather should come from the flexibility of simple control mechanisms, such as the sensory-motor loop. In this work we explore the possibility of equipping simple robots controlled by Boolean networks with the capability of modulating their sensory-motor loop such that their behavior adapts to the incumbent environmental conditions. This study builds upon the cybernetic concept of homeostasis, which is the property of maintaining essential parameters inside vital ranges, and analyzes the performance of adaptive mechanisms intervening in the sensory-motor loop. In particular, we focus on the possibility of maneuvering robot's effectors such that both their connections to network nodes and environmental features can be adapted. As the actions the robot takes have a feedback effect to its sensors mediated by the environment, this mechanism makes it possible to tune the sensory-motor loop, which, in turn, determines robot's behavior. We study this general setting in simulation and assess to what extent this mechanism can sustain the homeostasis of the robot. Our results show that controllers made of random Boolean networks in critical and chaotic regimes can be tuned such that their homeostasis in different environments is kept. This outcome is a step towards the design and deployment of controllers for micro-robots able to adapt to different environments."

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: '2024-04-16T11:30:00Z'
date_end: '2024-04-16T12:30:00Z'
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: '2018-01-01T00:00:00Z'

authors: []
tags: ["Past","LADE","Meeting"]

# Is this a featured talk? (true/false)
featured: false

image:
  caption: ''
  focal_point: Right

url_code: ''
url_pdf: ''
url_slides: ''
url_video: ''

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides:

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects:
---

<!-- Slides can be added in a few ways:

- **Create** slides using Wowchemy's [_Slides_](https://wowchemy.com/docs/managing-content/#create-slides) feature and link using `slides` parameter in the front matter of the talk file
- **Upload** an existing slide deck to `static/` and link using `url_slides` parameter in the front matter of the talk file
- **Embed** your slides (e.g. Google Slides) or presentation video on this page using [shortcodes](https://wowchemy.com/docs/writing-markdown-latex/).

Further event details, including page elements such as image galleries, can be added to the body of this page. -->
