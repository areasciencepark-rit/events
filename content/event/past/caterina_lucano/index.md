---
title: 'LADE Seminar'
subtitle: |
  Caterina Lucano (Orphanet)

  **Title**: Orphanet knowledge base on rare diseases and orphan drugs: know the rare for better care

event: LADE Seminar
event_url: 


location: Meeting Room S-20, Building C, Area Science Park
address:
  street: Località Padriciano 99
  city: Trieste
  region: 
  postcode: '34149'
  country: Italy

summary: Caterina Lucano (Orphanet)
abstract: "Rare diseases (RDs) are numerous, heterogeneous in nature, and geographically disparate. Few are preventable or curable, most are of pediatric-onset and may result in early death. While there is no universal definition of RDs, in the EU, the definition of RDs was established in the Regulation on orphan medicinal products (1999) as conditions whose prevalence is not more than 50 per 100 000. RDs are under-represented in healthcare coding systems. Only a small fraction of RDs have codes in international medical terminology standards, making it a challenge to trace patients with RDs in health information systems at a national and international level.
Orphanet has developed and maintains the Orphanet nomenclature of rare diseases, a unique and multilingual standardized system aimed at providing a specific terminology for rare diseases. Each clinical entity is assigned a unique and time-stable ORPHAcode, around which the rest of the data in the Orphanet knowledge base is structured. This clinical coding system provides a common language across healthcare and research systems for effective monitoring and reporting on rare diseases, thus improving their visibility.
By providing these services, Orphanet works towards meeting three main goals: i) improve the visibility of rare diseases in the fields of healthcare and research; ii) provide high-quality information on rare diseases, ensuring equal access to knowledge for all stakeholders; iii) contribute to generating knowledge on rare diseases ultimately improving the diagnostic pathway and clinical care provided to affected patients."

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: '2024-05-08T12:00:00Z'
date_end: '2024-05-08T13:00:00Z'
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: '2018-01-01T00:00:00Z'

authors: []
tags: ["Past","LADE","Seminar"]

# Is this a featured talk? (true/false)
featured: false

image:
  caption: ''
  focal_point: Right

url_code: ''
url_pdf: ''
url_slides: ''
url_video: ''

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides:

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects:
---

<!-- Slides can be added in a few ways:

- **Create** slides using Wowchemy's [_Slides_](https://wowchemy.com/docs/managing-content/#create-slides) feature and link using `slides` parameter in the front matter of the talk file
- **Upload** an existing slide deck to `static/` and link using `url_slides` parameter in the front matter of the talk file
- **Embed** your slides (e.g. Google Slides) or presentation video on this page using [shortcodes](https://wowchemy.com/docs/writing-markdown-latex/).

Further event details, including page elements such as image galleries, can be added to the body of this page. -->
