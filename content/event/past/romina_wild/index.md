---
title: 'LADE Seminar'
subtitle: |
  Romina Wild (SISSA)

  **Title**: Selecting and weighting informative features automatically with Differentiable Information Imbalance

event: LADE Seminar
event_url: 


location: Sala Riunioni RIT, Area Science Park
address:
  street: Località Padriciano 99
  city: Trieste
  region: 
  postcode: '34149'
  country: Italy

summary: Romina Wild
abstract: "Feature selection is a common practice in many applications and tends to come with attached 
uncertainties such as: What is the best dimensionality of a reduced feature space in order to retain maximum
information? How can one correct for different units of measure? What is the optimal relative scaling of
importance between features? To solve these questions, we extend Information Imbalance, an effective
statistic to rank information content between feature spaces, to its differentiable form. This Differentiable
Information Imbalance is used as a loss function to optimize relative feature weights, simultaneously per-
forming unit alignment and relative importance scaling. The same method can generate sparse solutions.
The optimal size of a reduced space is conveniently found by considering the change in the Differentiable 
Information Imbalance as a function of the number of non-zero features. "

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: '2024-04-17T11:30:00Z'
date_end: '2024-04-17T12:30:00Z'
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: '2018-01-01T00:00:00Z'

authors: []
tags: ["Past","LADE","Seminar"]

# Is this a featured talk? (true/false)
featured: false

image:
  caption: ''
  focal_point: Right

url_code: ''
url_pdf: ''
url_slides: ''
url_video: ''

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides:

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects:
---

<!-- Slides can be added in a few ways:

- **Create** slides using Wowchemy's [_Slides_](https://wowchemy.com/docs/managing-content/#create-slides) feature and link using `slides` parameter in the front matter of the talk file
- **Upload** an existing slide deck to `static/` and link using `url_slides` parameter in the front matter of the talk file
- **Embed** your slides (e.g. Google Slides) or presentation video on this page using [shortcodes](https://wowchemy.com/docs/writing-markdown-latex/).

Further event details, including page elements such as image galleries, can be added to the body of this page. -->
