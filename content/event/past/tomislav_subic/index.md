---
title: 'LADE Seminar'
subtitle: |
  Tomislav Šubić (Arctur)

  **Title**: The effects of model scaling on quantization error

event: LADE Seminar
event_url: 

location: Sala Riunioni RIT, Area Science Park
address:
  street: Località Padriciano 99
  city: Trieste
  region: 
  postcode: '34149'
  country: Italy

summary: Tomislav Šubić (Arctur)
abstract: "The performance and robustness of current artificial intelligence and machine learning systems heavily depend on their scale and the amount of compute spent on their training and inference. State-of-the-art models require vast amounts of energy, and compute has become one of the main contributors to greenhouse gas emissions. Additionally, due to the extensive costs connected to training and running these models, only a few global companies can afford to run them. This puts smaller companies and individuals at a disadvantage and centralizes the power of the technology; from that, a trend has emerged in the form of pre-trained model adoption.

Since most of the energy is spent on model inference, reducing its resource requirements is vital for running modern networks, from workstations and dedicated servers to devices with strict power and compute limits like edge and IoT devices. Several techniques exist for reducing the memory footprint of neural networks, where the goal is to compress a model without losing much of its accuracy.

Quantization of neural networks is a very active research area, but researchers often approach it with different goals. Some are converting neural networks into other numerical representations to increase the numerical range and increase accuracy; some do it as a simulation and test to drive hardware development, others try to reduce the memory footprint of a model and improve inference time by sacrifying accuracy. One of the questions that is left unanswered is that of how scaling a neural network influences the accuracy and performance of its quantized version. The ratios are not well understood; does a 10B parameter model represented in fp16 have the same accuracy as a quantized version of the scaled 20B parameter model? Which of these models would have better performance and energy efficiency? How much does a model need to be scaled and in what way, to achieve the same accuracy in its quantized form?

In this talk, we will take a look at the tradeoff between model scaling and quantization, present some papers that address these issues, and discuss the motivations behind optimizing this part of the AI/ML workflow."

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: '2024-02-06T11:30:00Z'
date_end: '2024-02-06T12:30:00Z'
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: '2018-01-01T00:00:00Z'

authors: []
tags: ["Past","LADE","Seminar"]

# Is this a featured talk? (true/false)
featured: false

image:
  caption: ''
  focal_point: Right

url_code: ''
url_pdf: ''
url_slides: ''
url_video: ''

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides:

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects:
---

<!-- Slides can be added in a few ways:

- **Create** slides using Wowchemy's [_Slides_](https://wowchemy.com/docs/managing-content/#create-slides) feature and link using `slides` parameter in the front matter of the talk file
- **Upload** an existing slide deck to `static/` and link using `url_slides` parameter in the front matter of the talk file
- **Embed** your slides (e.g. Google Slides) or presentation video on this page using [shortcodes](https://wowchemy.com/docs/writing-markdown-latex/).

Further event details, including page elements such as image galleries, can be added to the body of this page. -->
