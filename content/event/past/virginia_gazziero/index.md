---
title: 'LADE Meeting'
subtitle: |
  Virginia Gazziero

  **Title**: Automatization pipeline for FAIRification and processing of data from a NovaSeq6000 sequencing machine

event: LADE Meeting
event_url: 

location: Sala Riunioni RIT, edificio AM, Area Science Park
address:
  street: Località Padriciano 99,
  city: Trieste
  region: 
  postcode: '34149'
  country: Italy

summary: Virginia Gazziero
abstract: "The advent of Illumina technologies has revolutionized the biology field, and nowadays they stand out as the gold standard for both DNA and RNA sequencing. Since these methodologies generate a huge amount of data, appropriate data-flow management procedures must be integrated within these systems. Therefore, we created and implemented a multi-layered automatization architecture that aligns to the FAIR principles (Findability, Accessibility, Interoperability, and Reusability) for a Illumina-NovaSeq6000 sequencing machine and its processes. In particular, the defined system focuses on the demultiplexing and bcl to fastq format conversion steps. The pipeline described in the talk is based on the usage of Jenkins, an open-source automation server, which represented an extremely useful resource for the project. In addition, the described architecture is also available for ongoing and future developments and implementations for the ORFEO cluster."

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: '2023-11-22T11:30:00Z'
date_end: '2023-11-22T12:30:00Z'
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: '2018-01-01T00:00:00Z'

authors: []
tags: ["Past","LADE","Meeting"]

# Is this a featured talk? (true/false)
featured: false

image:
  caption: ''
  focal_point: Right

url_code: ''
url_pdf: ''
url_slides: ''
url_video: ''

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides:

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects:
---

<!-- Slides can be added in a few ways:

- **Create** slides using Wowchemy's [_Slides_](https://wowchemy.com/docs/managing-content/#create-slides) feature and link using `slides` parameter in the front matter of the talk file
- **Upload** an existing slide deck to `static/` and link using `url_slides` parameter in the front matter of the talk file
- **Embed** your slides (e.g. Google Slides) or presentation video on this page using [shortcodes](https://wowchemy.com/docs/writing-markdown-latex/).

Further event details, including page elements such as image galleries, can be added to the body of this page. -->
