---
title: 'LADE Seminar'
subtitle: |
  Edoardo Sarti (INRIA)

  **Title**: Understanding the function of paralogous protein sequences

event: LADE Seminar
event_url: 


location: Meeting Room S-20, Building C, Area Science Park
address:
  street: Località Padriciano 99
  city: Trieste
  region: 
  postcode: '34149'
  country: Italy

summary: Edoardo Sarti (INRIA)
abstract: "One of the main ways organisms evolve new functional proteins is via duplication events in their genome. When two copies of the same gene are present, either the organism benefits of a larger concentration of the expressed protein, or the sequence of one of the two copies will accumulate mutations and diverge in evolution, often developing new functions. Annotating the function of such paralogous sequences has always been very challenging both in small-scale, expert-guided assays and in large-scale bioinformatics studies, where paralogs are the most important source of functional annotation errors. 
ProfileView is a recent computational method designed to functionally classify sets of protein sequences. It constructs a library of probabilistic models accurately representing the functional variability of protein families, and extracts biologically interpretable information from the classification process. We have used it in order to classify the paralogs of the 11 proteins participating in the Calvin-Benson cycle (CBC), and obtained fully consistent results on 8 of them, and partially consistent results on other 2. The knowledge about paralog function annotation in the CBC is being now employed for matching same-function paralog sequences for producing joint MSAs for protein-protein interaction studies."

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: '2024-05-07T11:30:00Z'
date_end: '2024-05-07T12:30:00Z'
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: '2018-01-01T00:00:00Z'

authors: []
tags: ["Past","LADE","Seminar"]

# Is this a featured talk? (true/false)
featured: false

image:
  caption: ''
  focal_point: Right

url_code: ''
url_pdf: ''
url_slides: ''
url_video: ''

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides:

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects:
---

<!-- Slides can be added in a few ways:

- **Create** slides using Wowchemy's [_Slides_](https://wowchemy.com/docs/managing-content/#create-slides) feature and link using `slides` parameter in the front matter of the talk file
- **Upload** an existing slide deck to `static/` and link using `url_slides` parameter in the front matter of the talk file
- **Embed** your slides (e.g. Google Slides) or presentation video on this page using [shortcodes](https://wowchemy.com/docs/writing-markdown-latex/).

Further event details, including page elements such as image galleries, can be added to the body of this page. -->
