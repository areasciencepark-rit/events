---
title: 'LADE Meeting'
subtitle: |
  Lucrezia Valeriani

  **Title**: Copy Number Alteration Calling, DNA Methylation, and the Role of Oxford Nanopore Sequencing

event: LADE Meeting
event_url: 

location: Sala Riunioni RIT, edificio AM, Area Science Park
address:
  street: Località Padriciano 99,
  city: Trieste
  region: 
  postcode: '34149'
  country: Italy

summary: Lucrezia Valeriani
abstract: "Copy Number Alterations (CNAs) play a vital role in cancer genomics, representing abnormal DNA segment gains or losses influencing cancer development. Detecting CNAs accurately is crucial but challenging due to tumor heterogeneity and genomic complexity.
Traditional CNA analysis yields an averaged view at the chromosome level. Yet, recent technologies enable haplotype-specific CNA detection, differentiating alleles within an individual's genome. Integrating DNA methylation data offers a comprehensive perspective on epigenetic changes in haplotype-specific CNAs.
Oxford Nanopore long-read sequencing technology facilitates concurrent analysis of mutations, methylation, and CNAs within the same data promising a comprehensive view of CNA events. The talk will introduce these concepts and present preliminary results."

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: '2023-11-15T11:30:00Z'
date_end: '2023-11-15T12:30:00Z'
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: '2018-01-01T00:00:00Z'

authors: []
tags: ["Past","LADE","Meeting"]

# Is this a featured talk? (true/false)
featured: false

image:
  caption: ''
  focal_point: Right

url_code: ''
url_pdf: ''
url_slides: ''
url_video: ''

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides:

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects:
---

<!-- Slides can be added in a few ways:

- **Create** slides using Wowchemy's [_Slides_](https://wowchemy.com/docs/managing-content/#create-slides) feature and link using `slides` parameter in the front matter of the talk file
- **Upload** an existing slide deck to `static/` and link using `url_slides` parameter in the front matter of the talk file
- **Embed** your slides (e.g. Google Slides) or presentation video on this page using [shortcodes](https://wowchemy.com/docs/writing-markdown-latex/).

Further event details, including page elements such as image galleries, can be added to the body of this page. -->
