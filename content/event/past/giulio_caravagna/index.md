---
title: 'LADE Seminar'
subtitle: |
  Giulio Caravagna (UniTS)

  **Title**: Timing the origin of immune-escape from leukaemia after bone marrow transplant

event: LADE Seminar
event_url: 


location: Meeting Room S-20, Building C1, Area Science Park
address:
  street: Località Padriciano 99
  city: Trieste
  region: 
  postcode: '34149'
  country: Italy

summary: Giulio Caravagna (UniTS)
abstract: "In treating Acute Myeloid Leukaemia (AML), a bone marrow transplant introduces healthy donor stem cells to replace the leukemia-afflicted marrow, aiming to regenerate a healthy blood and immune system. Transplant works by triggering an immune response where donor immune cells attack residual leukaemia cells, beneficially contributing to AML treatment. However, AML cells can evade this immune attack through various mechanisms allowing them to hide from the immune system. In some type of transplants, immune evasion can be achieved by a complex structural rearrangement of the chromosome encoding for the locus used by the immune system to recognise AML cells. Like in all treatment-resistant scenarios, one of the most important question is understanding whether the treatment-resistant population was born before, or after treatment. In this talk I will approach this question thanks to a combination of longitudinal high-resolution whole-genome sequencing data, combined with mathematical models from Poisson processes, population genetics and Bayesian inference. By modelling the complex interplay between genomic mutations, structural alterations and drug-exposure, we were able to time the origin of the treatment-resistant clone in 10 distinct patients. For the first time, these results shed light on a complex process of immune-evasion using advanced computational modelling, focusing on one of the most aggressive type of leukaemia and offering a paradigmatic approach to be translated to other types of cancer."

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: '2024-02-21T11:30:00Z'
date_end: '2024-02-21T12:30:00Z'
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: '2018-01-01T00:00:00Z'

authors: []
tags: [Past","LADE","Seminar"]

# Is this a featured talk? (true/false)
featured: false

image:
  caption: ''
  focal_point: Right

url_code: ''
url_pdf: ''
url_slides: ''
url_video: ''

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides:

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects:
---

<!-- Slides can be added in a few ways:

- **Create** slides using Wowchemy's [_Slides_](https://wowchemy.com/docs/managing-content/#create-slides) feature and link using `slides` parameter in the front matter of the talk file
- **Upload** an existing slide deck to `static/` and link using `url_slides` parameter in the front matter of the talk file
- **Embed** your slides (e.g. Google Slides) or presentation video on this page using [shortcodes](https://wowchemy.com/docs/writing-markdown-latex/).

Further event details, including page elements such as image galleries, can be added to the body of this page. -->
