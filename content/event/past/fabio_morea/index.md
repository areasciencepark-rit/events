---
title: 'LADE Meeting'
subtitle: |
  Fabio Morea

  **Title**: Navigating Networks and Communities with Data Science

event: LADE Meeting
event_url: 

location: Sala Riunioni RIT, Area Science Park
address:
  street: Località Padriciano 99
  city: Trieste
  region: 
  postcode: '34149'
  country: Italy

summary: Fabio Morea
abstract: "Networks are anywhere around us, in social, business or academic contexts. Whenever individuals, organizations, or companies seek to connect with others, a network is established, and evolves over time as partnerships shift or new leaders emerge.

Over the past 20 year researchers have developed a theoretical framework and a large set of tools to explore networks’ structure. Meanwhile, an increasing number of complex and large datasets has become available, enabling the study of networks that extend over long periods of time and involve thousands of actors and connections.

This seminar, titled 'Navigating Networks and Communities with Data Science', provides a general overview of the topic, and a novel framework for unsupervised community detection focusing on the improvement of stability and quantification of uncertainty. Examples will be taken from a comprehensive dataset provided by the European Commission, which describes companies and research organizations that participated in the Horizon Framework Programmes over a period of 15 years.
 The seminar is based on the research conducted by the Author within the frame of a PhD in Applied Data Science and Artificial Intelligence (www.adsai.it) under the supervision of prof. Domenico De Stefano. "

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: '2024-04-23T11:30:00Z'
date_end: '2024-04-23T12:30:00Z'
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: '2018-01-01T00:00:00Z'

authors: []
tags: ["Past","LADE","Meeting"]

# Is this a featured talk? (true/false)
featured: false

image:
  caption: ''
  focal_point: Right

url_code: ''
url_pdf: ''
url_slides: ''
url_video: ''

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides:

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects:
---

<!-- Slides can be added in a few ways:

- **Create** slides using Wowchemy's [_Slides_](https://wowchemy.com/docs/managing-content/#create-slides) feature and link using `slides` parameter in the front matter of the talk file
- **Upload** an existing slide deck to `static/` and link using `url_slides` parameter in the front matter of the talk file
- **Embed** your slides (e.g. Google Slides) or presentation video on this page using [shortcodes](https://wowchemy.com/docs/writing-markdown-latex/).

Further event details, including page elements such as image galleries, can be added to the body of this page. -->
