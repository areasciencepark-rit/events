---
title: 'LADE Seminar: Ali Hassanali (ICTP)'
subtitle: Overcoming Discrete Notions in the Physical Chemistry of Water

event: LADE Seminar
event_url: 

location: Edificio C1-S20, Centro Congressi, Area Science Park
address:
  street: Località Padriciano 99
  city: Trieste
  region: 
  postcode: '34149'
  country: Italy

summary: Ali Hassanali (ICTP)
abstract: 'Is Liquid Water really a tale of two liquids? Is the structure of the excess proton in water an Eigen or Zundel species? Can molecules dissolved in water be rigidly classified into hydrophobic or hydrophilic? These are just a small sample of discrete/binary classifications which serve as fundamental theories in the physical chemistry of aqueous solutions. But how seriously should we take them? In this discussion, I will share a couple of stories that have recently emerged from our group that put these notions under the microscope.
I will illustrate some recent work leveraging a battery of advanced data-driven techniques that is giving us a new language to investigate the chemical physics of solvation in a wide variety of contexts. These agnostic approaches unequivocally show that water at finite temperature cannot be considered as a two component (LDL vs HDL) liquid. Furthermore, waters consistent ion, the proton, a key player in determining pH, cannot be rationalized in terms of idealized chemical structures, the Eigen nor a Zundel species. I will then elucidate how data- driven approaches allow us to reach beyond our chemical imagination and to understand the importance of geometry in solvation thermodynamics as well as in the collective dynamics of liquid water.
I will conclude with perspectives on some challenges and open problems in the field within the context of projects we are currently working on in the group.'

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: '2023-10-11T11:30:00Z'
date_end: '2023-10-11T12:30:00Z'
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: '2017-01-01T00:00:00Z'

authors: []
tags: ["Past","LADE","Seminar"]

# Is this a featured talk? (true/false)
featured: false

image:
  caption: ''
  focal_point: Right

url_code: ''
url_pdf: ''
url_slides: ''
url_video: ''

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides:

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects:
---

<!-- Slides can be added in a few ways:

- **Create** slides using Wowchemy's [_Slides_](https://wowchemy.com/docs/managing-content/#create-slides) feature and link using `slides` parameter in the front matter of the talk file
- **Upload** an existing slide deck to `static/` and link using `url_slides` parameter in the front matter of the talk file
- **Embed** your slides (e.g. Google Slides) or presentation video on this page using [shortcodes](https://wowchemy.com/docs/writing-markdown-latex/).

Further event details, including page elements such as image galleries, can be added to the body of this page. -->
