---
title: 'LADE Meeting'
subtitle: |
  Emanuele Panizon

  **Title**: Memory and control: Bits and bobs of understanding.

event: LADE Meeting
event_url: 

location: Sala Riunioni RIT, Area Science Park
address:
  street: Località Padriciano 99
  city: Trieste
  region: 
  postcode: '34149'
  country: Italy

summary: Emanuele Panizon
abstract: "Reinforcement Learning deals with how to act optimally to perform a predetermined task. Often, however, the agent lacks the complete information of the environment it is trying to manipulate: The instantaneous sensorial inputs, e.g., may not be sufficient to have an optimal (or even half-decent) performance. In this case the task may require storing past observations. But how to learn what should be retained in memory, and what should be quickly forgotten?
Using the task of olfactory search, common in nature to animals from insects to rodents, I will try to give a general sense of how the algorithmic construction of memory may be included and exploited in the reinforcement learning framework. "

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: '2024-03-26T11:30:00Z'
date_end: '2024-03-26T12:30:00Z'
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: '2018-01-01T00:00:00Z'

authors: []
tags: ["Past","LADE","Meeting"]

# Is this a featured talk? (true/false)
featured: false

image:
  caption: ''
  focal_point: Right

url_code: ''
url_pdf: ''
url_slides: ''
url_video: ''

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides:

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects:
---

<!-- Slides can be added in a few ways:

- **Create** slides using Wowchemy's [_Slides_](https://wowchemy.com/docs/managing-content/#create-slides) feature and link using `slides` parameter in the front matter of the talk file
- **Upload** an existing slide deck to `static/` and link using `url_slides` parameter in the front matter of the talk file
- **Embed** your slides (e.g. Google Slides) or presentation video on this page using [shortcodes](https://wowchemy.com/docs/writing-markdown-latex/).

Further event details, including page elements such as image galleries, can be added to the body of this page. -->
