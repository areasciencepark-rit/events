---
title: 'PRP Scientific Exchange Seminars'

# event: PRP seminar
# event_url: 

location: Meeting room S-20, building C1
address:
  street: Area Science Park, Località Padriciano 99
  city: Trieste
  region: 
  postcode: '34149'
  country: Italy

summary: Federica Piccirilli (Area Science Park), Luca Puricelli (Area Science Park) and Diana Bedolla Orozco (ICGEB)
# abstract: "TBD"

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: '2023-12-12T14:30:00Z'
date_end: '2023-12-12T16:30:00Z'
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: '2018-01-01T00:00:00Z'

authors: []
tags: ["Past","PRP","Seminar"]

# Is this a featured talk? (true/false)
featured: false

image:
  caption: ''
  focal_point: Right

url_code: ''
url_pdf: ''
url_slides: ''
url_video: ''

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides:

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects:
---

## Program


14:30 - 15:10: Talk by ***Federica Piccirilli (Area Science Park)***

**Title**: "Nanoresolved infrared microspectroscopy for biological studies"

15:10 - 15:50: Talk by ***Luca Puricelli (Area Science Park)***

**Title**: "AFM biomechanics at different scales: from tissues to cell membranes"

15:50 - 16:30:  Talk by ***Diana Bedolla Orozco (ICGEB)***

**Title**: "Tackling biomedical questions by using infrared spectroscopy and other techniques"

<!-- Slides can be added in a few ways:

- **Create** slides using Wowchemy's [_Slides_](https://wowchemy.com/docs/managing-content/#create-slides) feature and link using `slides` parameter in the front matter of the talk file
- **Upload** an existing slide deck to `static/` and link using `url_slides` parameter in the front matter of the talk file
- **Embed** your slides (e.g. Google Slides) or presentation video on this page using [shortcodes](https://wowchemy.com/docs/writing-markdown-latex/).

Further event details, including page elements such as image galleries, can be added to the body of this page. -->
