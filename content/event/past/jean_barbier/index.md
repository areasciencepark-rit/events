---
title: 'LADE Seminar'
subtitle: |
  Jean Barbier (ICTP)

  **Title**: Fundamental limits in structured principal component analysis and how to reach them

event: LADE Seminar
event_url: 


location: Meeting Room S-20, Building C1, Area Science Park
address:
  street: Località Padriciano 99
  city: Trieste
  region: 
  postcode: '34149'
  country: Italy

summary: Jean Barbier (ICTP)
abstract: "How does structure and statistical dependencies in the noise impact inference?  This talk will answer this question in the context of the estimation of low-rank matrices corrupted by structured noise, namely noise matrices with generic spectrum, and thus dependencies among its entries. We show that the Approximate Message Passing (AMP) algorithm currently proposed in the literature for Bayesian estimation is sub-optimal. We explain the reason for this sub-optimality and as a consequence we deduce an optimal Bayesian AMP algorithm with a rigorous state evolution matching our prediction for the minimum mean-square error. 
Based on a joint work with Francesco Camilli, Marco Mondelli and Manuel Saenz: https://www.pnas.org/doi/abs/10.1073/pnas.2302028120?doi=10.1073/pnas.2302028120"

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: '2024-04-10T11:30:00Z'
date_end: '2024-04-10T12:30:00Z'
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: '2018-01-01T00:00:00Z'

authors: []
tags: ["Past","LADE","Seminar"]

# Is this a featured talk? (true/false)
featured: false

image:
  caption: ''
  focal_point: Right

url_code: ''
url_pdf: ''
url_slides: ''
url_video: ''

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides:

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects:
---

<!-- Slides can be added in a few ways:

- **Create** slides using Wowchemy's [_Slides_](https://wowchemy.com/docs/managing-content/#create-slides) feature and link using `slides` parameter in the front matter of the talk file
- **Upload** an existing slide deck to `static/` and link using `url_slides` parameter in the front matter of the talk file
- **Embed** your slides (e.g. Google Slides) or presentation video on this page using [shortcodes](https://wowchemy.com/docs/writing-markdown-latex/).

Further event details, including page elements such as image galleries, can be added to the body of this page. -->
