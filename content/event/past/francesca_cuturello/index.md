---
title: 'LADE Meeting'
subtitle: |
  Francesca Cuturello

  **Title**: Evolution guides Protein Language Models for predicting stability variations upon single mutations

event: LADE Meeting
event_url: 

location: Sala Riunioni RIT, Area Science Park
address:
  street: Località Padriciano 99
  city: Trieste
  region: 
  postcode: '34149'
  country: Italy

summary: Francesca Cuturello
abstract: "Protein Language Models effectively address biological challenges in structure prediction relying exclusively on sequence information. Recent works investigate their application for predicting thermodynamic stability changes induced by single amino acid mutations, a notoriously complicated task due to the limited size and variability of the available datasets caused by experimental constraints. In this study, we introduce a strategy for predicting stability changes based on the fine-tuning of several pre-trained protein language models on a recently released mega-scale dataset. Our findings reveal that the MSA Transformer, leveraging explicitly the co-evolution signal encoded in homologous sequences, surpasses existing methods and exhibits enhanced generalization power. We define a stringent filtering pipeline to curate the datasets employed for training and testing the models to prevent overfitting. Our results highlight the robustness of our approach and its potential role in future developments in protein stability prediction without relying on structural information."

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: '2024-02-14T11:30:00Z'
date_end: '2024-02-14T12:30:00Z'
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: '2018-01-01T00:00:00Z'

authors: []
tags: ["Past","LADE","Meeting"]

# Is this a featured talk? (true/false)
featured: false

image:
  caption: ''
  focal_point: Right

url_code: ''
url_pdf: ''
url_slides: ''
url_video: ''

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides:

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects:
---

<!-- Slides can be added in a few ways:

- **Create** slides using Wowchemy's [_Slides_](https://wowchemy.com/docs/managing-content/#create-slides) feature and link using `slides` parameter in the front matter of the talk file
- **Upload** an existing slide deck to `static/` and link using `url_slides` parameter in the front matter of the talk file
- **Embed** your slides (e.g. Google Slides) or presentation video on this page using [shortcodes](https://wowchemy.com/docs/writing-markdown-latex/).

Further event details, including page elements such as image galleries, can be added to the body of this page. -->
