---
title: 'LADE Seminar: Fabio Anselmi (UniTS)'
subtitle: 'Machine Learning Catalysis of quantum tunneling'

event: LADE Seminar
event_url: 

location: Edificio C1-S20, Centro Congressi, Area Science Park
address:
  street: Località Padriciano 99
  city: Trieste
  region: 
  postcode: '34149'
  country: Italy

summary: Fabio Anselmi (UniTS)
abstract: 'Optimizing the probability of quantum tunneling between two states, while keeping the resources of the underlying physical system constant, is a task of key importance due to its critical role in various applications.
We show that, by applying Machine Learning techniques when the system is coupled to an ancilla, one optimizes the parameters of both the ancillary component and the coupling, ultimately resulting in the maximization of the tunneling probability.
We provide illustrative examples for the paradigmatic scenario involving a two-mode system and a two-mode ancilla in the presence of several interacting particles.
We also argue that the enhancement of the tunneling probability is not hampered by weak coupling to noisy
environments.'

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: '2023-10-18T11:30:00Z'
date_end: '2023-10-18T12:30:00Z'
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: '2017-01-01T00:00:00Z'

authors: []
tags: ["Past","LADE","Seminar"]

# Is this a featured talk? (true/false)
featured: false

image:
  caption: ''
  focal_point: Right

url_code: ''
url_pdf: ''
url_slides: ''
url_video: ''

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides:

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects:
---

<!-- Slides can be added in a few ways:

- **Create** slides using Wowchemy's [_Slides_](https://wowchemy.com/docs/managing-content/#create-slides) feature and link using `slides` parameter in the front matter of the talk file
- **Upload** an existing slide deck to `static/` and link using `url_slides` parameter in the front matter of the talk file
- **Embed** your slides (e.g. Google Slides) or presentation video on this page using [shortcodes](https://wowchemy.com/docs/writing-markdown-latex/).

Further event details, including page elements such as image galleries, can be added to the body of this page. -->
