---
title: 'LADE Meeting'
subtitle: |
  Fiorella Fabris

  **Title**: Intermittent oscillations of molecular signals in embryonic stem cells

event: LADE Meeting
event_url: 

location: Sala Riunioni RIT, Area Science Park
address:
  street: Località Padriciano 99
  city: Trieste
  region: 
  postcode: '34149'
  country: Italy

summary: Fiorella Fabris
abstract: "Although it is known that the differentiation of mouse preimplantation embryo cells and embryonic stem cells requires ERK activation, its short-term signaling dynamics in this biological context was previously unexplored. Taking an interdisciplinary approach, we build the first quantitative and theoretical description of short-term ERK activity dynamics in embryonic stem cells.
 
We first obtained time series that measures ERK activity in single cells. By time series analysis strategies that we developed in this work, we found that ERK activity consists in a novel type of dynamics, that we termed intermittent oscillations: intervals of oscillations interspersed with silent intervals and isolated pulses. We formalized this quantitative description with a minimal mathematical model, and we compare theory with experiments by fittings based on probabilistic sequential inference methods."

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: '2024-01-31T11:30:00Z'
date_end: '2024-01-31T12:30:00Z'
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: '2018-01-01T00:00:00Z'

authors: []
tags: ["Past","LADE","Meeting"]

# Is this a featured talk? (true/false)
featured: false

image:
  caption: ''
  focal_point: Right

url_code: ''
url_pdf: ''
url_slides: ''
url_video: ''

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides:

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects:
---

<!-- Slides can be added in a few ways:

- **Create** slides using Wowchemy's [_Slides_](https://wowchemy.com/docs/managing-content/#create-slides) feature and link using `slides` parameter in the front matter of the talk file
- **Upload** an existing slide deck to `static/` and link using `url_slides` parameter in the front matter of the talk file
- **Embed** your slides (e.g. Google Slides) or presentation video on this page using [shortcodes](https://wowchemy.com/docs/writing-markdown-latex/).

Further event details, including page elements such as image galleries, can be added to the body of this page. -->
