---
title: 'LADE Seminar'
subtitle: |
  Raffaello Potestio, University of Trento - Trento

  **Title**: Soft matter physics as a joint between renormalisation group and deep learning


event: LADE Seminar
event_url: 


location: Meeting Room S-20, Building C, Area Science Park
address:
  street: Località Padriciano 99
  city: Trieste
  region: 
  postcode: '34149'
  country: Italy

summary: Raffaello Potestio - University of Trento

abstract: "The ideas at the heart of the renormalisation group (RG) have greatly influenced many areas of Physics, in particular field theory and statistical mechanics. In the course of the past few decades, the field of coarse-graining in soft matter (SM) has developed at an increasingly high pace, leveraging RG-like methods and tools to build simple yet realistic representations of biological and artificial macromolecules, materials, and complex systems. The button-up parametrisation of low-resolution models and the study of a system's properties in terms of its coarse representations have greatly benefited from the theoretical machinery of RG. More recently, machine learning (ML) is entering the field of soft matter modelling as a key player, as both an instrument and an object of study. In this talk, I will present a few examples of the interplay between SM, RG, and ML, and discuss possible avenues for further developments."

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: '2024-06-05T11:30:00Z'
date_end: '2024-06-05T12:30:00Z'
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: '2018-01-01T00:00:00Z'

authors: []
tags: ["Recent","LADE","Seminar"]

# Is this a featured talk? (true/false)
featured: false

image:
  caption: ''
  focal_point: Right

url_code: ''
url_pdf: ''
url_slides: ''
url_video: ''

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides:

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects:
---

<!-- Slides can be added in a few ways:

- **Create** slides using Wowchemy's [_Slides_](https://wowchemy.com/docs/managing-content/#create-slides) feature and link using `slides` parameter in the front matter of the talk file
- **Upload** an existing slide deck to `static/` and link using `url_slides` parameter in the front matter of the talk file
- **Embed** your slides (e.g. Google Slides) or presentation video on this page using [shortcodes](https://wowchemy.com/docs/writing-markdown-latex/).

Further event details, including page elements such as image galleries, can be added to the body of this page. -->
