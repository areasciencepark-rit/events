---
title: 'LADE Meeting'
subtitle: |
  Marco Prenassi

  **Title**: Ontologies in biomedical research

event: LADE Meeting
event_url: 

location: Sala Riunioni RIT, Area Science Park
address:
  street: Località Padriciano 99
  city: Trieste
  region: 
  postcode: '34149'
  country: Italy

summary: Marco Prenassi
abstract: "Libraries are collection of books and form an important and ancient pillar of knowledge. The books contain information, most of them at least, because they spin snippet of data, real or imaginary, into a thread, that if followed could be weaved into a tale or a complex reasoning. In the modern world, other than libraries, we have immense, ever growing, data-lakes, most of them containing not books but raw data, disordered. Some structure could be given to these lakes, molding them into Data-warehouses, but the overseeing database schema is often only used to easily find and retrieve files, seldom it provides a map to the knowledge inside. To trace this map, we use ontologies, as they embrace entire warehouses stitching together the atomic bits of information using meaningful relations to create new meanings.
The objective of this meeting is to show how to use the strength of ontologies on top of well-defined database schemas to retrieve information from public biomedical repositories.
The example proposed will start from a simple statistical overview of some dataset and use it as a starting point to find the stories of the people inside through the use of ontologies.
This deep dive into data will be carried out using various tools such as MIMIC-IV, a patient medical record public repository, annotators, and search methodologies in the main standardized biomedical ontologies (e.g., Genomic Ontology (GO), and Protein Ontology, PRO)."

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: '2024-05-28T11:30:00Z'
date_end: '2024-05-28T12:30:00Z'
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: '2018-01-01T00:00:00Z'

authors: []
tags: ["Recent","LADE","Meeting"]

# Is this a featured talk? (true/false)
featured: false

image:
  caption: ''
  focal_point: Right

url_code: ''
url_pdf: ''
url_slides: ''
url_video: ''

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides:

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects:
---

<!-- Slides can be added in a few ways:

- **Create** slides using Wowchemy's [_Slides_](https://wowchemy.com/docs/managing-content/#create-slides) feature and link using `slides` parameter in the front matter of the talk file
- **Upload** an existing slide deck to `static/` and link using `url_slides` parameter in the front matter of the talk file
- **Embed** your slides (e.g. Google Slides) or presentation video on this page using [shortcodes](https://wowchemy.com/docs/writing-markdown-latex/).

Further event details, including page elements such as image galleries, can be added to the body of this page. -->
