---
title: 'Area Science Park/IRCCS Burlo Garofalo '
subtitle: |
  Presentation of scientific collaboration 

  Research lines and collaboration management methods

event: Meeting
event_url: 

location: Conference Hall, C1, Area Science Park
address:
  street: Località Padriciano 99
  city: Trieste
  region: 
  postcode: '34149'
  country: Italy

summary: Round table
abstract: "The meeting will focus on the scientific research lines of the institutes, the methods for starting and managing collaborative projects. Following the welcome greetings from the President of Area Science Park, Caterina Petrillo, the representatives from Area Science Park and IRCCS Burlo Garofalo will take the floor. The meeting will conclude with a discussion and exchange of ideas."

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: '2024-06-27T09:00:00Z'
date_end: '2024-06-27T13:00:00Z'
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: '2018-01-01T00:00:00Z'

authors: []
tags: ["Recent","Seminar"]

# Is this a featured talk? (true/false)
featured: false

image:
  caption: ''
  focal_point: Right

url_code: ''
url_pdf: ''
url_slides: ''
url_video: ''

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides:

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects:
---

