---
title: 'LADE Seminar'
subtitle: |
  Alessandra Magistrato, CNR-IOM/SISSA

  **Title**: Molecular Mechanisms of RNA Metabolism

event: LADE Seminar
event_url: 


location: Meeting Room S-20, Building C, Area Science Park
address:
  street: Località Padriciano 99
  city: Trieste
  region: 
  postcode: '34149'
  country: Italy

summary: Alessandra Magistrato, CNR-IOM/SISSA
abstract: |
  Complex protein/RNA macromolecular engines, catalyze formation of mature
  messenger and ribosomal RNAs from their precursors, giving rise to functional RNAs.
  In this talk I will illustrate how atomic-level simulations have contributed to elucidate:
  (i) the function of specific protein components of the spliceosome assembly [1,2]; (ii)
  the chemical details of premature-messenger and premature-ribosomal RNA splicing
  [3-4], and (iii) the mechanism of spliceosome helicases [5], and (iv) can contribute to
  refine the cryo-EM structure of complex RNA-based machineries.

  <br>
  <ol style="font-size: small">
  <li> Casalino, L.; Palermo, G.; Spinello, A.; Rothlisberger, U.; Magistrato, A. All-atom simulations disentangle the functional dynamics underlying gene maturation in the intron lariat spliceosome. Proc. Natl. Acad. Sci. U. S. A. 2018, 115, 6584 </li>
  <li> Saltalamacchia, A.; Casalino, L.; Borišek, J.; Batista, V. S.; Rivalta, I.; Magistrato, A. Decrypting the Information Exchange Pathways across the Spliceosome Machinery. J. Am. Chem. Soc. 2020, 142, 8403</li>
  <li> Borišek, J.; Magistrato, A. All-Atom Simulations Decrypt the Molecular Terms of RNA Catalysis in the Exon-Ligation Step of the Spliceosome. ACS Catal. 2020, 10, 5328</li>
  <li> J Aupič, J Borišek, SM Fica, WP Galej, A Magistrato Monovalent metal ion binding promotes the first transesterification reaction in the spliceosome Nature Communications 2023 14 (1), 8482</li>
  <li> Movilla, S.; Roca, M.; Moliner, V.; Magistrato, A Molecular Basis of RNA-Driven ATP Hydrolysis in DExH-Box Helicases. J. Am. Chem. Soc. 2023, https://doi.org/10.1021/jacs.2c11980"</li>
  </ol>


# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: '2024-06-13T11:30:00Z'
date_end: '2024-06-13T12:30:00Z'
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: '2018-01-01T00:00:00Z'

authors: []
tags: ["Recent","LADE","Seminar"]

# Is this a featured talk? (true/false)
featured: false

image:
  caption: ''
  focal_point: Right

url_code: ''
url_pdf: ''
url_slides: ''
url_video: ''

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides:

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects:
---

<!-- Slides can be added in a few ways:

- **Create** slides using Wowchemy's [_Slides_](https://wowchemy.com/docs/managing-content/#create-slides) feature and link using `slides` parameter in the front matter of the talk file
- **Upload** an existing slide deck to `static/` and link using `url_slides` parameter in the front matter of the talk file
- **Embed** your slides (e.g. Google Slides) or presentation video on this page using [shortcodes](https://wowchemy.com/docs/writing-markdown-latex/).

Further event details, including page elements such as image galleries, can be added to the body of this page. -->
