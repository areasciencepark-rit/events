---
# Leave the homepage title empty to use the site title
title:
date: 2022-10-24
type: landing


sections:
  - block: markdown
    content:
      title: 
      text: |
        <figure>
          <img align = "right" src="icon_r.png">
        </figure> 
        
        Here you can find the calendar of scientific events, meetings and workshops organized at [Area Science Park](https://en.areasciencepark.it/).
  - block: markdown
    content:
      title: Events
      image:
      text: |
        - **LADE Seminars**: Biweekly meetings with invited talks on research topics of interests for the Laboratory of Data Engineering (LADE). [Subscribe to announcements](#crm)!
        <br>
        - **LADE Meetings**: Biweekly meetings dedicated to LADE members progress reports and dedicated discussion on topics of interest.
        <br>
        - **PRP Seminars**: With this seminar series, scientists are given the opportunity to present their scientific focus and/or scientific background with the purpose to underline their expertise to be exploited within our Pathogens Preparedness network for new multidisciplinary research collaborations.
    design:
      columns: '2'
  - block: collection
    id: upcoming
    content:
      title: Upcoming Events
      filters:
        folders:
          - event
        tag: 'Upcoming'
      order: asc
    design:
      columns: '2'
      view: compact
  - block: collection
    id: recent
    content:
      title: Recent Events
      filters:
        folders:
          - event
        tag: 'Recent'
      order: desc
    design:
      columns: '2'
      view: compact
  - block: markdown
    id: crm
    content:
      title: Subscriptions
      text: |
        Subscribe to LADE Seminars announcements:
        
        <iframe src="https://analytics-eu.clickdimensions.com/areascienceparkit-apjd6/pages/acj0txsyee6beqaisjk6mq.html?PageId=b5f4c8699874ee118179002248993a99" allowtransparency="true" width="100%" height="500px" type="text/html" frameborder="0" style="border:0"></iframe>
    design:
      columns: '2'
  - block: contact
    id: contact
    content:
      title: Contact
      subtitle:
      text: |-

      # Contact (add or remove contact options as necessary)
      email: scientific.events@areasciencepark.it
      phone: +39 040 375 51 11
      appointment_url: 
      address:
        street: Località Padriciano 99
        city: Trieste
        region: 
        postcode: '34149'
        country: Italy
        country_code: IT
      coordinates:
        latitude: '45.65752'
        longitude: '13.82781'

      directions: 
      office_hours:
      contact_links:
      # Automatically link email and phone or display as text?
      autolink: true
    design:
      columns: '2'

  # - block: hero
  #   content:
  #     title: |
  #       Events @ RIT
  #     image:
  #       filename: welcome.jpg
  #     text: |
  #       <br>
        
  #       On this webpage you can find events organized and promoted by the Institute of Research and Technological Innovation (RIT) at [AREA Science Park](https://www.areasciencepark.it/), Trieste. 
  
#   - block: collection
#     content:
#       title: Latest News
#       subtitle:
#       text:
#       count: 5
#       filters:
#         author: ''
#         category: ''
#         exclude_featured: false
#         publication_type: ''
#         tag: ''
#       offset: 0
#       order: desc
#       page_type: post
#     design:
#       view: list
#       columns: '2'
  
#   - block: slider
#     content:
#       slides:
#       - title: LAME
#         content: Laboratory of Electronic Microscopy
#         align: center
#         background:
#           image:
#             filename: lame.jpg
#             filters:
#               brightness: 0.5
#           position: right
#           color: '#666'
#         link:
#           text: visit
#           url: ../test-website/#lame
#       - title: LADE
#         content: Laboratory of Data Engineering
#         align: center
#         background:
#           image:
#             filename: welcome.jpg
#             filters:
#               brightness: 0.5
#           position: center
#           color: '#555'
#         link:
#           text: visit
#           url: ../test-website/#lade
#       - title: LAGE
#         content: Laboratory of Genomics and Epigenomics
#         align: center
#         background:
#           image:
#             filename: lage.jpg
#             filters:
#               brightness: 0.5
#           position: center
#           color: '#333'
#         link:
#           text: visit
#           url: ../test-website/#lage
#     design:
#       # Slide height is automatic unless you force a specific height (e.g. '400px')
#       slide_height: ''
#       is_fullscreen: true
#       # Automatically transition through slides?
#       loop: true
#       # Duration of transition between slides (in ms)
#       interval: 4000

#   - block: markdown
#     id: lage
#     content: 
#       title: LAGE 
#       subtitle: Laboratory of Genomics and Epigenomics
#       text: |       
#         The Genomics and Epigenomics Laboratory (LAGE) works on analysing DNA and RNA sequences and genotyping (microarrays). Based on an open-access model, the laboratory organises, integrates and optimises resources, expertise and experience in a single unit. It is a leading national centre in the field of genomics and epigenomics and promotes synergies between regional, national and international players.

#         People:

#         Projects:
#     design:
#       columns: '1'

#   - block: markdown
#     id: lade
#     content: 
#       title: LADE 
#       subtitle: Laboratory of Data Engineering
#       text: |
#         The Data Engineering Laboratory manages and analyses data generated by the genomics and microscopy laboratories ([LAGE](../test-website/#lage) and [LAME](../test-website/#lame)), and carries out research and development in the field of artificial intelligence to extract knowledge and value from data. The core element is ORFEO, a datacenter that manages and analyses scientific data in an open-source framework.

#         People: [Alessio Ansuini](author/alessio-ansuini), [Matteo Biagetti](author/matteo-biagetti), [Alberto Cazzaniga](author/alberto-cazzaniga)

#         Projects: 
#     design:
#       columns: '1'

#   - block: markdown
#     id: lame
#     content: 
#       title: LAME 
#       subtitle: Laboratory of Electron Microscopy
#       text: |
#         The Electron Microscopy Laboratory, established in 2022, is focused on advanced characterisation of materials. It will be fully operational from 2025 and will operate in complete synergy with the research infrastructures (such as [Elettra Sincrotrone](https://en.areasciencepark.it/tenant/elettra-sincrotrone-trieste/) and [FERMI](https://en.areasciencepark.it/tenant/elettra-sincrotrone-trieste/)), institutes and companies located at Area Science Park. Equipped with advanced tools, LAME will carry out research programmes in collaboration with academia and industry and with national and international organisations, to provide open access to advanced tools in the field of electron microscopy.

#         People:

#         Projects:
#     design:
#       columns: '1'
  
#   - block: markdown
#     content:
#       title:
#       subtitle:
#       text: |
#         {{% cta cta_link="./people/" cta_text="Meet the team →" %}}
#     design:
#       columns: '1'
---