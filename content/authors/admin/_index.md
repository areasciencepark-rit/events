---
# Display name
title: Area Science Park - RIT

# Full name (for SEO)
first_name: Area Science Park - RIT
last_name: 

# Status emoji
# status:
#   icon: 

# Is this the primary user of the site?
superuser: true

# Role/position/tagline
role: Research Institute

# Organizations/Affiliations to show in About widget
organizations:
  - name: AREA Science Park
    url: https://en.areasciencepark.it/

# Short bio (displayed in user profile at end of posts)
bio: The Institute of Research and Innovation Technology (RIT) at Area Science Park carries out cutting-edge research and provides services and consulting to public and private-sector users through its three laboratories equipped with state-of-the-art technology.

# Interests to show in About widget
interests:


# Education to show in About widget
education:


# Social/Academic Networking
# For available icons, see: https://wowchemy.com/docs/getting-started/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:


# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ''

# Highlight the author in author lists? (true/false)
highlight_name: true

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
---

